import taichi as ti

ti.init(arch=ti.gpu, device_memory_GB=3, kernel_profiler=True)

from ti_rt import *
from example.article import scene

FPS = 30
size = width, height = 1920 // 1, 1080 // 1
camera = Pinhole(size)
# camera = Parallel(size)
screen = Screen(scene, camera)
screen.renderDepth = False
screen.renderNormal = False
screen.preActivate()

if __name__ == "__main__":
    screen.autoRender(FPS)
    # screen.staticRender(10)
    # ti.profiler.print_scoped_profiler_info()
    # ti.profiler.print_kernel_profiler_info()
