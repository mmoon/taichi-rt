import os


def isCode(line: str) -> bool:
    """判断一行字符串是否为有效代码"""
    # sourcery skip: assign-if-exp, boolean-if-exp-identity, reintroduce-else, remove-unnecessary-cast
    line = line.strip().lower()
    if len(line) < 3:
        return False
    if line.startswith('#'):
        return False
    if line.startswith('"') and line.endswith(('"', '",')):
        return False
    if line.startswith('@'):
        return False
    if line.startswith(("import ", "from ")):
        return False
    if line.startswith(("def ", "class ")):
        return False
    if line.startswith(('.', '-', '+')):
        return False
    if line.startswith(('(', '[', '"', "'", ')')):
        return False
    if not (line[0].islower() or line.startswith('_')):
        return False
    if line.endswith(','):
        return False
    return True


def countRow(filepath: str) -> int:
    with open(filepath, encoding="utf-8") as code:
        return sum(map(isCode, code))


def countRows(fp: list[str]) -> dict[str, int]:
    """作行数统计"""
    return {
        f: countRow(f)
        for f in sum(
            (
                [os.path.join(f, ff) for ff in os.listdir(f) if ff.endswith(".py")]
                if os.path.isdir(f)
                else [f]
                for f in fp
            ),
            [],
        )
    }


if __name__ == "__main__":
    allRow = countRows(
        [
            "./example/",
            "./ti_rt/cameras/",
            "./ti_rt/color/",
            "./ti_rt/helper/",
            "./ti_rt/materials/",
            "./ti_rt/shapes/",
            "./ti_rt/utils/",
            "./ti_rt/",
            "./count_rows.py",
            "./path_tracing.py",
        ]
    )
    print("行数统计:")
    for file in allRow:
        print(f" - {file}: {allRow[file]}")
    print(f"总行数: {sum(allRow[i] for i in allRow)}")
