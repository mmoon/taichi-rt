import taichi as ti, numpy as np

ti.init(arch=ti.gpu)
from ti_rt import *
from tqdm import tqdm

size = width, height = 1920 // 1, 1080 // 1
camera = Pinhole(size)
ball = (
    Sphere(5)
    .setVelocity(0.985)
    .setMaterial(Diffuse().setColor(CheckerSphere(RED_C, GREEN_C, freq=[15, 10])))
)
scene = Scene(
    [
        ball,
    ],
    SkyBox("./res/background/ice").sample,
)
screen = Screen(scene, camera).preActivate()

# video = ti.tools.VideoManager("output/", framerate=30, automatic_build=False)
# for i, v in enumerate(tqdm(np.linspace(0, .985**2, 200))):
#     v = v**0.5
#     ball.setVelocity(v)
#     camera.tp(-10 * v, 0, -10, 0, np.pi / 2, np.pi * (1 / 2))
#     # camera.tp(10 * 3**0.5 - (20 - 1) * v, 0, -10, 0, np.pi / 2, np.pi * (5 / 6))
#     camera.setScreen()
#     screen.sampleN(1024)
#     screen.mapping()
#     video.write_frame(screen.image)
#     screen.clearBuffer()
# video.make_video(gif=False)

# camera.tp(10 * 3**0.5, 0, -10, 0, np.pi / 2, np.pi * (5 / 6))
screen.autoRender(30)
