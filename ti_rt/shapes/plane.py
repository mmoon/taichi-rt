import taichi as ti

from ..shape import Shape
from ..utils import Vec3, Inf, eps, Ray3


class Plane(Shape):
    """平面"""

    @ti.func
    def intersect(self: int, ray: Ray3, closest: float):
        root, norm = Inf, Vec3(0, 0, 1)
        if ray.direct.z != 0:
            root_ = -ray.origin.z / ray.direct.z
            if root_ >= eps:
                root = root_
        return root, norm
