import taichi as ti

from .plane import Plane
from ..utils import Inf, Ray3


class Circle2d(Plane):
    """平面圆"""

    def __init__(self, r: float):
        super().__init__()
        self.r = r
        Plane.shapes[self.index].attribute[0] = r

    @ti.func
    def intersect(self: int, ray: Ray3, closest: float):
        r = Plane.shapes[self].attribute[0]
        dist, norm = Plane.intersect(self, ray, closest)
        if dist < closest:
            hitPos = ray.at(dist)
            if hitPos.x**2 + hitPos.y**2 > r**2:
                dist = Inf
        return dist, norm
