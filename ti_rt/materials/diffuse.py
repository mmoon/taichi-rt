import taichi as ti

from ..material import Material
from ..utils import Vec3, Ray3, toNormalHemisphere, sampleCosineHemisphere


class Diffuse(Material):
    """漫反射材质"""

    @ti.func
    def scatter(self: int, ray: Ray3, hitPoint: Vec3, norm: Vec3):
        ray.origin = hitPoint
        color = Material.getColor(self, hitPoint)
        if ray.direct.dot(norm) > 0:
            norm *= -1
        ray.direct = toNormalHemisphere(sampleCosineHemisphere(), norm)
        return color, ray


Material.default = Diffuse()
