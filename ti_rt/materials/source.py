import taichi as ti

from ..material import Material
from ..utils import Vec3, Ray3


class Source(Material):
    """光源材质"""

    asEnd = True

    @ti.func
    def scatter(self: int, ray: Ray3, hitPoint: Vec3, norm: Vec3):
        color = Material.getColor(self, hitPoint)
        return color, ray
