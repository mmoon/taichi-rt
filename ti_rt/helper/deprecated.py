import inspect
from functools import wraps
from typing import Callable

from .log import Logger


def deprecated(reason: str | Callable):
    if isinstance(reason, str):

        def decorator(func: Callable):
            @wraps(func)
            def new_func1(*args, **kwargs):
                Logger.warn(
                    f"Call to deprecated {'class' if inspect.isclass(func) else 'function'} {func.__name__} ({reason})."
                )
                return func(*args, **kwargs)

            return new_func1

        return decorator
    else:

        @wraps(reason)
        def new_func2(*args, **kwargs):
            Logger.warn(
                f"Call to deprecated {'class' if inspect.isclass(reason) else 'function'} {reason.__name__}."
            )
            return reason(*args, **kwargs)

        return new_func2
