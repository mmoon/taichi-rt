import numpy as np


class MovingAverage:
    def __init__(self, window: int):
        self.window = window
        self.data = np.ndarray(self.window)
        self.num = 0

    def next(self, x: float) -> float:
        if self.num < self.window:
            self.data[self.num] = x
            self.num += 1
        else:
            self.data[:-1] = self.data[1:]
            self.data[-1] = x
        return self.data[: self.num].mean()
