from itertools import product
from ti_rt import *

mat_brdf1 = DisneyBRDF(0, 1).setColor(0.3, 0.3, 0.3)
mat_brdf2 = DisneyBRDF(1, 0).setColor(1, 1, 1)
mat_light = Source().setColor(0, 1, 0)
mat_brdf3 = DisneyBRDF(0.9, 0.1).setColor(1, 0.1, 0.1)
mat_brdf4 = DisneyBRDF(0.3, 1).setColor(0.1, 0.1, 1)
# mat_brdf5 = BRDF(0.01, 0.9).setColor(1, 1, 1)
mat_brdf5 = DisneyBRDF(0.01, 1).setColor(1, 1, 1)
mat_brdf6 = DisneyBRDF(0, 1).setColor(1, 1, 0.1)
mat_brdf7 = DisneyBRDF(1, 1).setColor(1, 0, 1)
mat_d1 = Dielectric().setColor(1, 1, 1)
mat_d2 = Dielectric(1.778).setColor(0, 1, 1)

# brdf_table = [
#     Sphere(0.3).setPosition(i, j, 10).setMaterial(BRDF(i / 5, j / 5).setColor(0, 1, 1))
#     for i, j in product(range(5), range(5))
# ]

brdf_ball1 = Sphere(100).setPosition(0, -100.5, 0).setMaterial(mat_brdf1)
brdf_ball2 = Sphere(0.5).setPosition(0, 0, 0).setMaterial(mat_brdf2)
brdf_ball3 = Sphere(0.3).setPosition(1, -0.2, 0).setMaterial(mat_brdf3)
brdf_ball4 = Sphere(0.3).setPosition(-1, -0.2, 0).setMaterial(mat_brdf4)
brdf_block = Cuboid(2, 1, 0.2).setPosition(0, 0, -1).setMaterial(mat_brdf5)
brdf_cy1 = Cylinder(0.2, 1).setPosition(0, -0.5, 1).setMaterial(mat_brdf6)
brdf_cy2 = Cylinder(0.5, 1.2).setPosition(0, -0.5, -2).setMaterial(mat_brdf7)
ball1 = Sphere(0.3).setPosition(-1, -0.2, 1).setMaterial(mat_d1)
ball2 = Sphere(0.3).setPosition(1, -0.2, 1).setMaterial(mat_d2)

sky = SkyBox("res/background/ice/")
# sky = SkySphere("res/hdr/Tokyo_BigSight_3k.hdr")

scene = Scene(  # brdf_table
    [
        brdf_ball1,
        brdf_ball2,
        brdf_ball3,
        brdf_ball4,
        brdf_block,
        brdf_cy1,
        brdf_cy2,
        ball1,
        ball2,
    ],
    sky.sample,
)
