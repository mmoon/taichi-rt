import taichi as ti
from ti_rt import *

gd = (
    Rectangle2d(128, 128)
    .setPosition(y=-4)
    .rotateBy([1, 0, 0], 90)
    .setMaterial(
        Metal(0.01).setColor(Grid(BILIBILI_PINK, freq=(1, 1, 0), offset=(0, 0, 0.5)))
    )
)
wall = (
    Rectangle2d(128, 128)
    .setPosition(z=4)
    .setMaterial(Diffuse().setColor(Checker(color2=BILIBILI_BLUE, freq=(1, 1, 0))))
)
light = Cuboid(8, 4, 0.5).setPosition(z=4.1).setMaterial(Source().setColor(10, 10, 10))
glass = (
    Cuboid(8.8, 4.8, 0.9)
    .setPosition(z=4.1)
    .setMaterial(Dielectric(1.5).setColor(0.9, 0.6, 0.3))
)


Mercury = getSpherical(getImage("res/texture/mercury.jpg"))
Venus = getSpherical(getImage("res/texture/venus.jpg"))
Earth = getSpherical(getImage("res/texture/earth.jpg"))
Mars = getSpherical(getImage("res/texture/mars.jpg"))
Jupiter = getSpherical(getImage("res/texture/jupiter.jpg"))
Saturn = getSpherical(getImage("res/texture/saturn.jpg"))
Uranus = getSpherical(getImage("res/texture/uranus.jpg"))
Neptune = getSpherical(getImage("res/texture/neptune.jpg"))
planes = [
    Sphere(0.5)
    .setPosition(-1.5 * i, -3)
    .setVelocity(0.99)
    .setMaterial(Diffuse().setColor(texture))
    # .setMaterial(Diffuse().setColor(CheckerSphere(color2=texture, freq=[5, 5])))
    for i, texture in enumerate(
        (Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune)
        # (RED_E, ORANGE, YELLOW_E, GREEN_E, BLUE_E, TEAL_E, PURPLE_E)
    )
]
scene = Scene([gd, wall, light, glass] + planes, SkyBox("res/background/ice/").sample)
