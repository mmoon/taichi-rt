from numpy.random import rand
from ti_rt.utils import PI, Vec3
from ti_rt import *

matMirror = DisneyBRDF(roughness=0, metallic=1).setColor(*MetalIron)

plane2 = (
    Circle2d(50)
    .setPosition(0, -1, 0)
    .setMaterial(DisneyBRDF().setColor(0.8, 0.6, 0.4))
    .rotateBy((1, 0, 0), 90)
)
mirror2 = Rectangle2d(2, 3).setPosition(0, 0.5, 3).setMaterial(matMirror)
mirror2_ = Rectangle2d(2, 3).setPosition(0, 0.5, -3).setMaterial(matMirror)
lightSource2 = Sphere().setPosition(0, 5, 0).setMaterial(Source().setColor(8, 7, 6))

cube_series = [
    Cuboid()
    .setPosition(-4, 0, 2 * i)
    .setVelocity(0, 0, 0.985)
    .setMaterial(Metal().setColor(Checker(WHITE, c, [4, 4, 4])))
    for i, c in enumerate((RED_E, ORANGE, YELLOW_E, GREEN_E, BLUE_E, TEAL_E, PURPLE_E))
]
cube6_2 = Cuboid(1, 2, 3).setPosition(-5.05, 0, 0).setMaterial(Dielectric())

# keLi = loadModel("./res/model/可莉.obj")
# dragon = loadModel("./res/model/dragon.obj", sah=False)

scene = Scene(
    [
        plane2,
        lightSource2,
        mirror2,
        mirror2_,
        cube6_2,
        # loadModel("./res/model/可莉.obj").setMaterial(
        #     DisneyBRDF(roughness=0.5, metallic=0.5, clearCoat=0.1).setColor(
        #         0.623, 0.180, 0.109
        #     )
        # ),
        # dragon()
        # .setPosition(3, 0, 0)
        # .rotateBy(Vec3(0, 1, 0), PI / 2)
        # .setMaterial(DisneyBRDF(roughness=0.01, metallic=1).setColor(*color.MetalGold)),
    ]
    + cube_series,
    SkyBox("./res/background/ice").sample,
)
