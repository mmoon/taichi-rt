import taichi as ti
from ti_rt import *

gd = (
    Rectangle2d(128, 128)
    .setPosition(y=-4)
    .rotateBy([1, 0, 0], 90)
    .setMaterial(
        Metal(0.01).setColor(Grid(BILIBILI_PINK, freq=(1, 1, 0), offset=(0, 0, 0.5)))
    )
)
wall = (
    Rectangle2d(128, 128)
    .setPosition(z=4)
    .setMaterial(Diffuse().setColor(Checker(color2=BILIBILI_BLUE, freq=(1, 1, 0))))
)
light = Cuboid(8, 4, 0.5).setPosition(z=4.1).setMaterial(Source().setColor(10, 10, 10))
glass = (
    Cuboid(8.8, 4.8, 0.9)
    .setPosition(z=4.1)
    .setMaterial(Dielectric(1.5).setColor(0.9, 0.6, 0.3))
)

walls = [gd, wall, light, glass]

beta = .0
vary_objs = [
    # Sphere(.5).setPosition(0, -3).setVelocity(beta).setMaterial(Diffuse().setColor(CheckerSphere(color2=RED_E, freq=[5, 5]))),
    # Cuboid(1).setPosition(-2, -3).setVelocity(beta).setMaterial(Diffuse().setColor(Checker(color2=ORANGE, freq=[5, 5, 5]))),
    # Cylinder(.1).setPosition(-4, -3.5).setVelocity(beta).setMaterial(Diffuse().setColor(Checker(color2=YELLOW_E, freq=[5, 5, 5]))),
    # Rectangle2d(.6, .8).setPosition(-5, -3).setVelocity(beta).setMaterial(Diffuse().setColor(Checker(color2=GREEN_E, freq=[5, 5, 5]))).rotateBy([1, 0, 0], 45),
    # Triangle2d([.2, .6], [-.4, 1]).setPosition(-5.75, -3.2).setVelocity(beta).setMaterial(Diffuse().setColor(Checker(color2=BLUE_E, freq=[25, 25, 25]))).rotateBy([1, 0, 0], 45),
    # Circle2d(.5).setPosition(-6.25, -3).setVelocity(beta).setMaterial(Diffuse().setColor(Checker(color2=TEAL_E, freq=[25, 25, 25]))).rotateBy([1, 0, 0], 45),
]

planes = [
    # Sphere(0.5)
    # .setPosition(-2 * i, -3)
    # .setVelocity(0.99)
    # .setMaterial(Diffuse().setColor(CheckerSphere(color2=texture, freq=[5, 5])))
    # for i, texture in enumerate(
    #     (RED_E, ORANGE, YELLOW_E, GREEN_E, BLUE_E, TEAL_E, PURPLE_E)
    # )
]

objs = [
    # Sphere(1.5).setVelocity(.99).setMaterial(Diffuse().setColor(CheckerSphere(color2=ORANGE, freq=[5, 5]))),
    # loadModel("./res/model/可莉.obj")().setMaterial(
    #     DisneyBRDF(roughness=0.5, metallic=0.5, clearCoat=0.1).setColor(
    #         0.623, 0.180, 0.109
    #     )
    # ).setVelocity(.9).rotateBy([0, 1, 0], 240),
    # Cuboid().setVelocity(.9).setMaterial(Dielectric(1).setColor(Grid(color2=BLACK, freq=[5, 5, 5], width=.05))),
    Cuboid(3).setVelocity(.99).setMaterial(Diffuse().setColor(Checker(color2=ORANGE, freq=[5, 5, 5]))),
    # Cuboid(3).setVelocity(.7).setMaterial(Metal().setColor(1, 1, 1)),
    # Plane().setVelocity(0, 0, .7).setMaterial(Diffuse().setColor(Checker(color2=ORANGE, freq=[1, 1, 1]))),
    
]

scene = Scene(walls + planes + objs + vary_objs, SkyBox("res/background/ice/").sample)
