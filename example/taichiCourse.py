from ti_rt.utils import PI
from ti_rt import *

mat0 = Source().setColor(3, 3, 3)
mat1 = Diffuse().setColor(*LIGHT_GRAY)
mat2 = Diffuse().setColor(*RED_E)
mat3 = Diffuse().setColor(*GREEN_E)
mat4 = Diffuse().setColor(*BLUE_E)
mat5 = Metal().setColor(*MetalIron)
mat6 = Dielectric(1.5)
mat7 = Metal(0.4).setColor(*MetalGold)

lightSource = Sphere(3).setPosition(0, 4.4, 4).setMaterial(mat0)
frontWall = Plane().setPosition(0, 0, 6).setMaterial(mat1)
ground = Plane().setPosition(y=-1.5).setMaterial(mat1).rotateBy((1, 0, 0), 90)
ceiling = Plane().setPosition(y=1.5).setMaterial(mat1).rotateBy((1, 0, 0), 90)
rWall = Plane().setPosition(x=1.5).setMaterial(mat2).rotateBy((0, 1, 0), 90)
lWall = Plane().setPosition(x=-1.5).setMaterial(mat3).rotateBy((0, 1, 0), 90)
diffuseBall = Sphere(0.3).setPosition(0, -1.2, 3.5).setMaterial(mat4)
metalBall = Sphere(0.7).setPosition(0.8, -0.8, 4).setMaterial(mat5)
glassBall = Sphere(0.5).setPosition(-0.7, -1, 4.5).setMaterial(mat6)
metalBall_ = Sphere(0.2).setPosition(-0.6, -1.3, 3).setMaterial(mat7)

scene = Scene(
    [
        lightSource,
        frontWall,
        ground,
        ceiling,
        lWall,
        rWall,
        diffuseBall,
        metalBall,
        glassBall,
        metalBall_,
    ]
)
