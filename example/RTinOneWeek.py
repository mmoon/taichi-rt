from itertools import product
from random import random

from ti_rt.utils import Vec3
from ti_rt import *

mat_ground = Diffuse().setColor(0.5, 0.5, 0.5)
lst4 = [Sphere(1000).setPosition(0, -1000, 0).setMaterial(mat_ground)]

static_point = Vec3(4, 0.2, 0)
for a, b in product(range(-10, 10), range(-10, 10)):
    choose_mat = random()
    center = Vec3(a + 0.9 * random(), 0.2, b + 0.9 * random())

    if choose_mat < 0.8:
        if (center - static_point).norm() > 0.9:
            mat = Diffuse().setColor(random(), random(), random())
    elif choose_mat < 0.95:
        if (center - static_point).norm() > 0.9:
            mat = Metal(random() * 0.5).setColor(
                *(Vec3(random(), random(), random()) / 2 + 0.5)
            )
    elif (center - static_point).norm() > 0.9:
        mat = Dielectric(1.5)

    lst4.append(Sphere(0.2).setPosition(*center).setMaterial(mat))

mat1 = Dielectric(1.5)
mat2 = Diffuse().setColor(0.4, 0.2, 0.2)
mat3 = Metal(0).setColor(0.7, 0.6, 0.5)
lst4.append(Sphere().setPosition(0, 1, 0).setMaterial(mat1))
lst4.append(Sphere().setPosition(-4, 1, 0).setMaterial(mat2))
lst4.append(Sphere().setPosition(4, 1, 0).setMaterial(mat3))

scene = Scene(lst4)
