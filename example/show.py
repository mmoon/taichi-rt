from ti_rt.utils import PI
from ti_rt import *


matSource2 = Source().setColor(9, 8, 7)
mat20 = Diffuse().setColor(0.4, 0.3, 0.2)
matMirror = Metal().setColor(0.5, 0.5, 0.5)
matSky21 = Diffuse().setColor(0.5, 0.7, 1)
matDiff21 = Diffuse().setColor(0.1, 0.7, 0.1)
matDiff22 = Diffuse().setColor(0.1, 0.4, 0.9)
matNorm2 = Dielectric()
matStick = Diffuse().setColor(0.5, 0.2, 0.4)

plane2 = Circle2d(50).setPosition(0, -1, 0).setMaterial(mat20).rotateBy((1, 0, 0), 90)
mirror2 = Rectangle2d(2, 3).setPosition(0, 0.5, 3).setMaterial(matMirror)
mirror2_ = Rectangle2d(2, 3).setPosition(0, 0.5, -3).setMaterial(matMirror)
skyBall = Sphere().setPosition(4, 0, 0).setMaterial(matSky21)
cube2_1 = (
    Cuboid()
    .setPosition(-4, -0.1, 0)
    .setMaterial(matDiff21)
    # .rotateBy((0, 1, 0), PI / 4)
    # .rotateBy((1, 0, 0), PI / 2)
    # .rotateBy((0, 0, 1), 0.1)
    .setVelocity(0, 0, 0.99)
)
block2_1 = Cuboid(1, 2, 3).setPosition(-4, 0, 5).setMaterial(matDiff22)
ballNorm2 = Sphere(2).setPosition(8, 5, 0).setMaterial(matNorm2)
lightSource2 = Sphere().setPosition(0, 5, 0).setMaterial(matSource2)
tri = Triangle2d((3, 0), (0, 4)).setPosition(0, -1, -5).rotateBy((1, 0, 0), 45)
stick = (
    Cylinder(0.05, 2)
    .setPosition(-1, -0.5, 0)
    .setMaterial(matStick)
    .rotateBy((0, 0, 1), 45)
    .rotateBy((0, 1, 0), 45)
)

scene = Scene(
    (
        plane2,
        lightSource2,
        mirror2,
        mirror2_,
        skyBall,
        cube2_1,
        block2_1,
        ballNorm2,
        tri,
        stick,
    ),
    # darkSkyLike,
    SkyBox("res/background/ice/").sample,
)
