# 相对论X光线追踪

## 项目介绍

这是一个基于taichi的相对论性光线追踪引擎，通过在路径追踪技术中引入四维时空求交，以渲染狭义相对论被考虑的情况下，物体在观测者眼中展现的视觉形象。
![a](res/result/0.png)
![b](res/result/1.png)
![c](res/result/2.png)

## 项目功能

- 可渲染球体、长方体、圆柱面、平面圆、三角形以及由SDF（符号距离函数）定义的复杂形状的场景。
- 可渲染漫反射、镜面反射、折射（电介质）以及各向同性的Disney原理化BRDF。
- 通过修改变量可切换是否考虑狭义相对论进行渲染。
- 形状与材质各项参数，如位置、大小、粗糙度可在运行时动态调节（暂时需要手动调用 `Camera.clearBuffer()`清空缓冲区以获得正确表现）。
- 实时渲染（画面质量？不存在的）和离线渲染，相机参数如fov、位置、朝向可响应鼠标键盘操控（W: 前, S:后, A: 左, D: 右, Space(空格): 上, Shift: 下, E: 时间流逝, Q: 时间倒流, ALT: 加速移动, 按住鼠标右键: 移动视角）。
- 可以加载模型文件，并且建立BVH盒加速模型的渲染。

## 源代码结构

- example/: 存放一些示例场景
- ti_rt/: 主要逻辑代码存放位置
  - cameras/: 针孔、平行相机等的具体实现
  - color/: 颜色空间、着色函数、光谱渲染
  - helper/: 一些辅助代码存放位置
    - log.py: 统一格式打印日志的代码
  - materials/: 存放各种材质的具体实现
  - shapes/: 存放各种几何形状的具体实现
  - utils/: 一些工具函数、工具类
    - background.py: 实现天空盒、天球等背景着色
    - gauss.py: 实现高斯模糊
    - model.py: 实现模型加载
    - perlin.py: 柏林噪声生成器的实现 (WIP)
    - sdf.py: 实现SDF形状的底层实现
    - texture.py: 实现纹理贴图
  - camera.py: 定义相机的抽象接口
  - config.py: 全局配置文件
  - material.py: 定义材质的接口和底层实现
  - scene.py: 定义场景的具体实现
  - screen.py: 定义屏幕和GUI功能的实现
  - shape.py: 定义形状的接口和底层实现

## 食用教程

1. 使用 git 克隆本仓库至本地。

   ```bash
   git clone https://gitee.com/mmoon/taichi-rt.git
   ```
2. 使用pip安装 `requirements.txt`里包含的依赖，所标明版本为推荐而非强制。

   ```bash
   pip install -r requirements.txt
   ```
3. 用python运行 `path_tracing.py`文件开始程序。
4. 可以通过 `ti_rt.shapes`中定义的各个 `Shape`的子类创建形状实例，调用 `setPosition`等方法改变形状的属性，或通过 `ti_rt.materials`下的类创建材质并通过 `Shape.setMaterial`方法赋予形状。将准备好的形状放入一个 `Scene`中，再和一个设置好的 `Camera`一起交给一个 `Screen`，最后调用 `Screen.autoRender`方法即可开始渲染。
5. 关于更多技术细节请参考源代码实现。

## TODO

1. [X] 泛光的后处理效果。
2. [ ] 实时调整物体参数如位置、大小等的gui界面。
3. [ ] HDR局部色调映射。
4. [ ] `Scene`类保存为json并可从json读取。
5. [X] 光谱渲染。
6. [ ] 广义相对论光线步进追踪。
7. [ ] 双目立体视觉。

## 参考

1. [基于numpy的狭义相对论渲染](https://gitee.com/liangkeshulizi/relativistic-ray-tracer)
2. [A raytrace framework using taichi language](https://github.com/lyd405121/ti-raytrace)
3. [太极图形课S1-Ray Tracing示例程序](https://github.com/erizmr/taichi_ray_tracing)
4. [一周光线追踪系列](https://raytracing.github.io/)
5. [SDF 渲染](https://github.com/HK-SHAO/RayTracingPBR)
6. [迪士尼 BRDF 函数](https://github.com/wdas/brdf/blob/main/src/brdfs/disney.brdf)
7. [奇奇怪怪的相机](https://www.bilibili.com/read/cv17025322)
